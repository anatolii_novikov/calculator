module.exports = function (grunt) {
    'use strict';

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-require');
    grunt.loadNpmTasks('grunt-contrib-yuidoc');
    grunt.loadNpmTasks('grunt-jsdoc-ng');
    grunt.initConfig({
        copy: {
            postInstall: {
                files: [
                    {
                        src: 'node_modules/angular/angular.js',
                        dest: 'src/js/vendor/angular/angular.js',
                        filter: 'isFile'
                    },
                    {
                        src: 'node_modules/angular/angular.min.js.map',
                        dest: 'src/js/vendor/angular/angular.min.js.map',
                        filter: 'isFile'
                    },
                    {
                        src: 'node_modules/angular-ui-router/release/angular-ui-router.js',
                        dest: 'src/js/vendor/angular/angular-ui-router.js',
                        filter: 'isFile'
                    },
                    {
                        src: 'node_modules/requirejs/require.js',
                        dest: 'src/js/vendor/requirejs/require.js',
                        filter: 'isFile'
                    },
                    {
                        src: 'node_modules/domReady/domReady.js',
                        dest: 'src/js/vendor/requirejs-domready/domReady.js',
                        filter: 'isFile'
                    },
                    {
                        src: 'node_modules/bootstrap/dist/css/bootstrap.min.css',
                        dest: 'src/css/bootstrap.min.css',
                        filter: 'isFile'
                    }
                ]
            }
        },
        requirejs: {
            options: {
                baseUrl: "src/js/",
                out: "src/js/build/main.js",
                mainConfigFile: 'src/js/main.js',
                include: [
                    'vendor/requirejs/require.js',
                    'main'
                ],
                name: "main"
            },
            prod: {
                options: {
                    build: true
                }
            }
        },
        pkg: grunt.file.readJSON('package.json'),
        yuidoc: {
            compile: {
                name: '<%= pkg.name %>',
                description: '<%= pkg.description %>',
                version: '<%= pkg.version %>',
                options: {
                    paths: 'src/js/',
                    exclude: 'src/js/vendor/',
                    outdir: 'docs/'
                }
            }
        },
        'jsdoc-ng' : {
            'compile' : {
                src: ['src/js/'],
                dest: 'docs',
                options: {
                    "source": {
                        "exclude": [ 'src/js/vendor/' ]
                    },
                    "opts": {
                        "recurse": true
                    }
                }
            }
        }
    });

    grunt.registerTask('postInstall', ['copy:postInstall']);
    grunt.registerTask('compile', ['requirejs:prod']);
    grunt.registerTask('generateDocs', ['jsdoc-ng:compile']);
};