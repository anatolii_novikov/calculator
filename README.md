Calculator
==========

This project demonstrates my skills with JavaScript, AngularJS, RequireJs. It is very simple calculator.

Calculator uses algorithm ["Reverse Polish notation"](https://www.wikiwand.com/en/Reverse_Polish_notation) for result calculation.

### Installation

- Clone project.
- Use [npm](https://www.npmjs.com/) for installing dependencies.

```
npm install
```

### Running application

- Configure webroot of your favorite web-server for working in `src` directory.
- Another way you can use built in http-server. Go to project directory and run command. After that you can see calcalator in your browser by address [http://localhost:8080](http://localhost:8080)

```
npm start
```

### Running end to end tests

- These tests are run with the Protractor End-to-End test runner.
- Run built in http-server.

```
npm start
```

- Use another terminal for updating WebDriver.

```
npm run update-webdriver
```

- Run e2e tests

```
npm run protractor
```

### Deployment to production

- Compile all JS files to a single file

```
npm run compile
```

- Open `./src/index.html` in editor and replace line `11` to 

```
<script type="text/javascript" src="js/build/main.js"></script>
```
