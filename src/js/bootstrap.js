define([
    'require',
    'angular',
    './calculator',
    './routes'
], function (require, ng) {
    'use strict';

    require(['domReady!'], function (document) {

        // Setup angular
        ng.bootstrap(document, ['calculatorApplication']);
    });
});