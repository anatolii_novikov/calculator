define([
    'angular',
    'uiRouter',
    'modules/calculator/index'
], function (ng) {
    'use strict';

    // Load dependencies for Angular application
    return ng.module('calculatorApplication', [
        'ui.router',
        'calculator'
    ]);
});