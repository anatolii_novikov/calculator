require.config({
    baseUrl: 'js',

    // Aliases and paths for libraries and plug-ins
    paths: {
        domReady: './vendor/requirejs-domready/domReady',
        angular: './vendor/angular/angular',
        uiRouter: './vendor/angular/angular-ui-router'
    },

    // Angular doesn't supported AMD out of the box, so export angular variables to the global scope
    shim: {
        angular: {
            exports: 'angular'
        },
        uiRouter: {
            deps: ['angular']
        }
    },

    // run application
    deps: ['bootstrap']
});