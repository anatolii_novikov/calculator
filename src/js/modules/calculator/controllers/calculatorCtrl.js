/**
 * Main page controller. Calculator is located on the main page.
 */
define(['./module'], function (controllers) {
    'use strict';
    controllers.controller('calculatorCtrl', ['calculator', function (calculator) {

        // Initialization of variables
        var calculatorFormula = false,
            formulaBuilder = false,
            self = this;

        // Function for initialization environment of new calculation
        function initNewFormula() {
            calculatorFormula = calculator.getNewFormula();
            formulaBuilder = calculator.getFormulaBuilder();

            formulaBuilder.setFormula(calculatorFormula);
            self.display = 0;
            self.outputFormula = '';
            self.error = false;
        }

        this.display = 0;

        // Function for updating display after user action
        this.updateDisplay = function () {
            this.outputFormula = '';
            this.display = calculatorFormula.render();
            this.error = false;
        };

        // This function adds a new digit to formula
        this.addDigit = function (digit) {
            formulaBuilder.addDigit(digit);
            this.updateDisplay();
        };

        this.getFormulaBuilder = function () {
            return formulaBuilder;
        };

        // Action for button '='
        this.calculate = function () {
            var outputFormula = calculatorFormula.render(),
                display,
                charIndex;
            if (outputFormula) {
                try {
                    display = '= ' + calculatorFormula.calculate();
                    initNewFormula();
                    this.display = display;
                    this.outputFormula = outputFormula;

                    // Adds calculation result to new formula. It is need for reusing result in new calculation.
                    for (charIndex in display) {
                        if (display.hasOwnProperty(charIndex)) {
                            if (parseInt(display[charIndex], 10).toString() === display[charIndex]) {
                                formulaBuilder.addDigit(parseInt(display[charIndex], 10));
                            }
                            if (display[charIndex] === '.') {
                                formulaBuilder.addComma();
                            }
                        }
                    }
                } catch (error) {
                    // Show error of calculation
                    initNewFormula();
                    this.display = error.message;
                    this.outputFormula = outputFormula;
                    this.error = true;
                }
            }
        };

        // Action for button 'erase'
        this.clear = function () {
            initNewFormula();
        };

        // Start calculation
        initNewFormula();
    }]);
});