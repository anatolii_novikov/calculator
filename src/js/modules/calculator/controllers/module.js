define(['angular'], function (ng) {
    'use strict';

    // Define controllers for module
    return ng.module('calculator.controllers', []);
});