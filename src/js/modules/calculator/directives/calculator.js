/**
 * Directive adds new tag name <calculator> for calculator displaying
 */
define(['./module'], function (directives) {
    'use strict';
    directives.directive('calculator', function () {

        return {
            restrict: 'E',
            transclude: false,
            templateUrl: 'templates/modules/calculator/directives/calculator.html',
            controllerAs: 'calculator',
            scope: {},
            controller: 'calculatorCtrl'
        };
    });
});