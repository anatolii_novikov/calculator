define(['angular'], function (ng) {
    'use strict';

    // Define directives for module
    return ng.module('calculator.directives', []);
});