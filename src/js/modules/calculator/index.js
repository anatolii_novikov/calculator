/**
 * Define module calculator
 *
 * @module calculator
 */
define([
    'angular',
    './controllers/index',
    './services/index',
    './directives/index'
], function (ng) {
    'use strict';

    // Define module calculator
    return ng.module('calculator', [
        'calculator.controllers',
        'calculator.services',
        'calculator.directives'
    ]);
});