/**
 * Service provides methods for formula construction
 */
define([
    './module',
    './formula/formula',
    './formula/calculator',
    './formula/render',
    './formula/formulaBuilder'
], function (services, Formula, Calculator, Render, FormulaBuilder) {
    'use strict';

    services
        .factory('calculator', function () {

            return {
                /**
                 * Factory method for getting a new formula
                 *
                 * @return {Formula}
                 */
                getNewFormula: function () {
                    return new Formula(new Calculator(), new Render());
                },

                /**
                 * Factory method for getting a new formula builder
                 *
                 * @return {FormulaBuilder}
                 */
                getFormulaBuilder: function () {
                    return new FormulaBuilder();
                }
            };
        });
});