/**
 * Implements calculation algorithm. Class uses algorithm "Reverse Polish notation". For more details see {@link https://www.wikiwand.com/en/Reverse_Polish_notation | "Reverse Polish notation"}
 *
 * @class calculator
 * @constructor
 */
define([
    './elements/number',
    './elements/operations/abstractOperation'
], function (number, abstractOperation) {
    'use strict';

    var calculator = function () {
        this.outputElements = [];
        this.stack = [];
    };

    /**
     * Adds a new element for calculation. Converts infix expression in postfix expression
     *
     * @param element A new element for calculation. Must implement class abstractElement
     * @return {calculator}
     */
    calculator.prototype.addElement = function (element) {
        var checkStackTop = function () {
            if (element.getPriority() <= this.stack[this.stack.length - 1].getPriority()) {
                this.outputElements.push(this.stack.shift());
                if (this.stack.length) {
                    checkStackTop();
                }
            }
        }.bind(this);

        if (element instanceof number) {
            this.outputElements.push(element);
        }
        if (element instanceof abstractOperation) {
            if (this.stack.length === 0) {
                this.stack.push(element);
            } else {
                checkStackTop();
                this.stack.unshift(element);
            }
        }
        return this;
    };

    /**
     * Calculates result
     *
     * @return {Number} Result of calculation.
     */
    calculator.prototype.calculate = function () {
        if (this.stack.length > 0) {
            this.outputElements = this.outputElements.concat(this.stack);
        }
        var calculateOutput = function (inputElements) {
            var calculatedElements = [],
                elementIndex;
            for (elementIndex in inputElements) {
                if (inputElements.hasOwnProperty(elementIndex)) {
                    if (inputElements[elementIndex] instanceof abstractOperation) {
                        calculatedElements.push(inputElements[elementIndex].setFormulaStack(calculatedElements).calculate());
                    } else {
                        calculatedElements.push(inputElements[elementIndex]);
                    }
                }
            }
            if (calculatedElements.length > 1) {
                calculateOutput(calculatedElements);
            }
            return calculatedElements;
        },
            preparedElements = this.outputElements.map(function (element) {
                if (element instanceof number) {
                    return element.getValue();
                }
                return element;
            }),
            result = calculateOutput(preparedElements);
        return result[0];
    };


    return calculator;
});