define(function () {
    'use strict';
    /**
     * Abstract class represents an element of formula
     *
     * @class abstractElement
     * @constructor
     */
    var abstractElement = function () {
        return undefined;
    };

    /**
     * Renders the element
     *
     * @abstract
     * @return {String} Represents element for human view
     */
    abstractElement.prototype.render = function () {
        throw new Error('Abstract method "render"');
    };

    return abstractElement;
});