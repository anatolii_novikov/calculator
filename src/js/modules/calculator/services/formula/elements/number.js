/**
 * Class represents number elements
 *
 * @class number
 * @constructor
 * @extends abstractElement
 */
define(['./abstractElement'], function (abstractElement) {
    'use strict';
    var number = function () {
        abstractElement.apply(this, arguments);
        var numberElements = [],
            commaAdded = false;
        /**
         * Adds new digit to the number
         *
         * @throws If input has incorrect value
         * @throws If input does not belong to interval 0-9
         * @param {Number} digit Must be 0-9
         */
        this.addDigit = function (digit) {
            if (!(Number(digit) === digit && digit % 1 === 0)) {
                throw new Error('Input must be integer');
            }
            if (digit < 0 || digit > 9) {
                throw new Error('Input digit must be in intervar 0-9');
            }
            numberElements.push(digit);
        };

        /**
         * Adds a comma to the number
         *
         * @throws If user adds second comma
         * @throws If any digits have not been added before setting comma
         */
        this.addComma = function () {
            if (commaAdded) {
                throw new Error("Error, the second comma cann't be set");
            }
            numberElements.push('.');
            commaAdded = true;
        };

        this.getValue = function () {
            if (!numberElements.length) {
                throw new Error('Value was not set');
            }
            return parseFloat(numberElements.join(''));
        };
    };

    number.prototype = Object.create(abstractElement.prototype);

    number.prototype.render = function () {
        return this.getValue().toString();
    };


    return number;
});