/**
 * Abstract class represents an operation with numbers
 *
 * @class abstractOperation
 * @constructor
 * @extends abstractElement
 */
define(['./../abstractElement'], function (abstractElement) {
    'use strict';

    var abstractOperation = function () {
        abstractElement.apply(this, arguments);

        var stack = false;

        this.setFormulaStack = function (stackInput) {
            stack = stackInput;
            return this;
        };

        this.getFormulaStack = function () {
            if (stack === false) {
                throw new Error('Stack was not set');
            }
            return stack;
        };
    };

    abstractOperation.prototype = Object.create(abstractElement.prototype);

    /**
     * Checks count of numbers available for the operation
     *
     * @throws If formula doesn't have count of numbers required for the operation
     * @param number Digit represents count of numbers required for the operation
     */
    abstractOperation.prototype.checkNumberOperands = function (number) {
        if (this.getFormulaStack().length < number) {
            throw new Error('Wrong number of operands');
        }
    };

    /**
     * Get result of the operation
     *
     * @abstract
     * @return {Number} Result of the operation
     */
    abstractOperation.prototype.calculate = function () {
        throw new Error('Abstract method "calculate"');
    };

    /**
     * Get priority of the operation
     *
     * @abstract
     * @return {Number} Priority of the operation
     */
    abstractOperation.prototype.getPriority = function () {
        throw new Error('Abstract method "calculate"');
    };

    return abstractOperation;
});