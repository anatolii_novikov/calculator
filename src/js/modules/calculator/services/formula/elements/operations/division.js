/**
 * Class represents a division operation
 *
 * @class division
 * @constructor
 * @extends abstractOperation
 */
define(['./abstractOperation'], function (abstractOperation) {
    'use strict';

    var division = function () {
        abstractOperation.apply(this, arguments);
    };

    division.prototype = Object.create(abstractOperation.prototype);

    /**
     * Get result of division
     *
     * @throws Division by zero
     * @return {Number} Result of the operation
     */
    division.prototype.calculate = function () {
        this.checkNumberOperands(2);
        var divider = this.getFormulaStack().pop();
        if (divider === 0) {
            throw new Error('Division by zero');
        }
        return this.getFormulaStack().pop() / divider;
    };

    division.prototype.render = function () {
        return '/';
    };

    division.prototype.getPriority = function () {
        return 2;
    };

    return division;
});