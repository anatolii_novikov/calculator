/**
 * Class represents a minus operation
 *
 * @class minus
 * @constructor
 * @extends abstractOperation
 */
define(['./abstractOperation'], function (abstractOperation) {
    'use strict';

    var minus = function () {
        abstractOperation.apply(this, arguments);
    };

    minus.prototype = Object.create(abstractOperation.prototype);

    minus.prototype.calculate = function () {
        this.checkNumberOperands(2);
        var subtrahend = this.getFormulaStack().pop();
        return this.getFormulaStack().pop() - subtrahend;
    };

    minus.prototype.render = function () {
        return '-';
    };

    minus.prototype.getPriority = function () {
        return 1;
    };

    return minus;
});