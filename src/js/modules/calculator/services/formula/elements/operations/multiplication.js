/**
 * Class represents a multiplication operation
 *
 * @class multiplication
 * @constructor
 * @extends abstractOperation
 */
define(['./abstractOperation'], function (abstractOperation) {
    'use strict';

    var multiplication = function () {
        abstractOperation.apply(this, arguments);
    };

    multiplication.prototype = Object.create(abstractOperation.prototype);

    multiplication.prototype.calculate = function () {
        this.checkNumberOperands(2);
        return this.getFormulaStack().pop() * this.getFormulaStack().pop();
    };

    multiplication.prototype.render = function () {
        return '*';
    };

    multiplication.prototype.getPriority = function () {
        return 2;
    };

    return multiplication;
});