/**
 * Class represents a plus operation
 *
 * @class plus
 * @constructor
 * @extends abstractOperation
 */
define(['./abstractOperation'], function (abstractOperation) {
    'use strict';

    var plus = function () {
        abstractOperation.apply(this, arguments);
    };

    plus.prototype = Object.create(abstractOperation.prototype);

    plus.prototype.calculate = function () {
        this.checkNumberOperands(2);
        return this.getFormulaStack().pop() + this.getFormulaStack().pop();
    };

    plus.prototype.render = function () {
        return '+';
    };

    plus.prototype.getPriority = function () {
        return 1;
    };

    return plus;
});