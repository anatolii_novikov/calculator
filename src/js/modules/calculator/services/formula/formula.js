define(function () {
    'use strict';
    /**
     * Provides methods for operation with formula
     *
     * @class formula
     * @constructor
     * @param calculator Object implements calculation algorithm
     * @param render Object for rendering formula
     */
    var formula = function (calculator, render) {

        /**
         * Adds a new element to formula
         * @param element A new formula element. Must implement class abstractElement
         */
        this.addElement = function (element) {
            calculator.addElement(element);
            render.addElement(element);
        };

        /**
         * Get result
         *
         * @return {Number} Result of calculation
         */
        this.calculate = function () {
            return calculator.calculate();
        };

        /**
         *  Render the formula
         *
         *  @return {String} Represents formula for human view
         */
        this.render = function () {
            return render.render();
        };
    };

    return formula;
});