/**
 * Provides methods for adding operations and numbers to formula
 *
 * @class formulaBuilder
 * @constructor
 */
define([
    './elements/number',
    './elements/operations/division',
    './elements/operations/minus',
    './elements/operations/multiplication',
    './elements/operations/plus'
], function (NumberElement, Division, Minus, Multiplication, Plus) {
    'use strict';

    var formulaBuilder = function () {
        var currentNumber = false;

        this.getCurrentNumber = function () {
            if (!currentNumber) {
                currentNumber = new NumberElement();
                this.formula.addElement(currentNumber);
            }
            return currentNumber;
        };

        this.addCurrentNumber = function () {
            if (!currentNumber) {
                throw new Error('Number was not set.');
            }
            currentNumber = false;
        };
    };

    /**
     * Sets formula object for construction
     *
     * @param formula Formula object must implement formula class
     */
    formulaBuilder.prototype.setFormula = function (formula) {
        this.formula = formula;
    };

    /**
     * Adds digit to current number
     *
     * @param digit One digit for adding in current number
     * @return {formulaBuilder}
     */
    formulaBuilder.prototype.addDigit = function (digit) {
        this.getCurrentNumber().addDigit(digit);
        return this;
    };

    /**
     * Adds comma to current number
     *
     * @return {formulaBuilder}
     */
    formulaBuilder.prototype.addComma = function () {
        this.getCurrentNumber().addComma();
        return this;
    };

    /**
     * Adds division operation to formula
     *
     * @return {formulaBuilder}
     */
    formulaBuilder.prototype.addDivision = function () {
        this.addCurrentNumber();
        this.formula.addElement(new Division());
        return this;
    };

    /**
     * Adds minus operation to formula
     *
     * @return {formulaBuilder}
     */
    formulaBuilder.prototype.addMinus = function () {
        this.addCurrentNumber();
        this.formula.addElement(new Minus());
        return this;
    };

    /**
     * Adds multiplication operation to formula
     *
     * @return {formulaBuilder}
     */
    formulaBuilder.prototype.addMultiplication = function () {
        this.addCurrentNumber();
        this.formula.addElement(new Multiplication());
        return this;
    };

    /**
     * Adds plus operation to formula
     *
     * @return {formulaBuilder}
     */
    formulaBuilder.prototype.addPlus = function () {
        this.addCurrentNumber();
        this.formula.addElement(new Plus());
        return this;
    };

    return formulaBuilder;
});