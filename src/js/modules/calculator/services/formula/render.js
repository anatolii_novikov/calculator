define(function () {
    'use strict';

    /**
     * Implements render algorithm for displaying of formula
     *
     * @class render
     * @constructor
     */
    var render = function () {
        this.elements = [];
    };

    /**
     * Adds a new element for displaying
     * @param element A new formula element. Must implement class abstractElement
     * @return {render}
     */
    render.prototype.addElement = function (element) {
        this.elements.push(element);
        return this;
    };

    /**
     *
     * @return {String} Represents formula for human view
     */
    render.prototype.render = function () {
        return this.elements.reduce(function (previousValue, currentValue) {
            if (!previousValue) {
                return currentValue.render();
            }
            return previousValue + ' ' + currentValue.render();
        }, '');
    };

    return render;
});