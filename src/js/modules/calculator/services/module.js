define([
    'angular'
], function (ng) {
    'use strict';

    // Define services for module
    return ng.module('calculator.services', []);
});