define(['./calculator'], function (app) {
    'use strict';
    return app.config(['$stateProvider', '$locationProvider', '$urlRouterProvider', function ($stateProvider, $locationProvider, $urlRouterProvider) {

        $stateProvider

            // Configuring router for the main page. Calculator is located on the main page.
            .state('mainPage', {
                url: '/',
                templateUrl: 'templates/mainPage.html'
            });
        $urlRouterProvider.otherwise("/");
        $locationProvider.html5Mode(true);
    }]);
});