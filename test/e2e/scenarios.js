/**
 * End to end tests.
 */
(function () {
    'use strict';

    describe('Calculator', function () {

        var display = element(by.binding('calculator.display')),
            outputFormula = element(by.binding('calculator.outputFormula')),
            button_1 = element(by.buttonText("1")),
            button_2 = element(by.buttonText("2")),
            button_3 = element(by.buttonText("3")),
            button_4 = element(by.buttonText("4")),
            button_5 = element(by.buttonText("5")),
            button_6 = element(by.buttonText("6")),
            button_7 = element(by.buttonText("7")),
            button_8 = element(by.buttonText("8")),
            button_9 = element(by.buttonText("9")),
            button_0 = element(by.buttonText("0")),
            button_ce = element(by.buttonText("CE")),
            button_division = element(by.buttonText("/")),
            button_multiplication = element(by.buttonText("*")),
            button_minus = element(by.buttonText("-")),
            button_plus = element(by.buttonText("+")),
            button_calculate = element(by.buttonText("="));

        describe('Check calculator initialization', function () {
            it("Calculator display should be 0", function () {
                browser.get('/', 30000);
                expect(display.getText()).toBe('0');
            });
        });
        describe('Check buttons', function () {
            it('should show 1', function () {
                browser.actions().click(button_1).perform();
                expect(display.getText()).toBe('1');
            });

            it('should show 12', function () {
                browser.actions().click(button_2).perform();
                expect(display.getText()).toBe('12');
            });

            it('should show 123', function () {
                browser.actions().click(button_3).perform();
                expect(display.getText()).toBe('123');
            });

            it('should show 1234', function () {
                browser.actions().click(button_4).perform();
                expect(display.getText()).toBe('1234');
            });

            it('should show 12345', function () {
                browser.actions().click(button_5).perform();
                expect(display.getText()).toBe('12345');
            });

            it('should show 123456', function () {
                browser.actions().click(button_6).perform();
                expect(display.getText()).toBe('123456');
            });

            it('should show 1234567', function () {
                browser.actions().click(button_7).perform();
                expect(display.getText()).toBe('1234567');
            });

            it('should show 12345678', function () {
                browser.actions().click(button_8).perform();
                expect(display.getText()).toBe('12345678');
            });

            it('should show 123456789', function () {
                browser.actions().click(button_9).perform();
                expect(display.getText()).toBe('123456789');
            });

            it('should show 1234567890', function () {
                browser.actions().click(button_0).perform();
                expect(display.getText()).toBe('1234567890');
            });

            it('should show 0', function () {
                browser.actions().click(button_ce).perform();
                expect(display.getText()).toBe('0');
            });

            it('should show "1 + 1 - 1 * 1 /"', function () {
                browser.actions().click(button_1).perform();
                browser.actions().click(button_plus).perform();
                browser.actions().click(button_1).perform();
                browser.actions().click(button_minus).perform();
                browser.actions().click(button_1).perform();
                browser.actions().click(button_multiplication).perform();
                browser.actions().click(button_1).perform();
                browser.actions().click(button_division).perform();
                expect(display.getText()).toBe('1 + 1 - 1 * 1 /');
            });

        });

        describe('Check operations', function () {

            beforeEach(function () {
                browser.actions().click(button_ce).perform();
            });

            it('Check plus operation.', function () {
                browser.actions().click(button_1).perform();
                browser.actions().click(button_plus).perform();
                browser.actions().click(button_2).perform();
                browser.actions().click(button_calculate).perform();
                expect(display.getText()).toBe('= 3');
                expect(outputFormula.getText()).toBe('1 + 2');
            });

            it('Check minus operation.', function () {
                browser.actions().click(button_5).perform();
                browser.actions().click(button_minus).perform();
                browser.actions().click(button_1).perform();
                browser.actions().click(button_calculate).perform();
                expect(display.getText()).toBe('= 4');
                expect(outputFormula.getText()).toBe('5 - 1');
            });

            it('Check division operation.', function () {
                browser.actions().click(button_5).perform();
                browser.actions().click(button_division).perform();
                browser.actions().click(button_2).perform();
                browser.actions().click(button_calculate).perform();
                expect(display.getText()).toBe('= 2.5');
                expect(outputFormula.getText()).toBe('5 / 2');
            });

            it('Check multiplication operation.', function () {
                browser.actions().click(button_9).perform();
                browser.actions().click(button_multiplication).perform();
                browser.actions().click(button_7).perform();
                browser.actions().click(button_calculate).perform();
                expect(display.getText()).toBe('= 63');
                expect(outputFormula.getText()).toBe('9 * 7');
            });

            it('Check division by 0.', function () {
                browser.actions().click(button_5).perform();
                browser.actions().click(button_division).perform();
                browser.actions().click(button_0).perform();
                browser.actions().click(button_calculate).perform();
                expect(display.getText()).toBe('Division by zero');
                expect(outputFormula.getText()).toBe('5 / 0');
                expect(display.getAttribute('class')).toMatch('danger');
            });

            it('Check sequence of operations.', function () {
                browser.actions().click(button_5).perform();
                browser.actions().click(button_9).perform();
                browser.actions().click(button_division).perform();
                browser.actions().click(button_2).perform();
                browser.actions().click(button_calculate).perform();
                browser.actions().click(button_plus).perform();
                browser.actions().click(button_8).perform();
                browser.actions().click(button_calculate).perform();
                expect(display.getText()).toBe('= 37.5');
                expect(outputFormula.getText()).toBe('29.5 + 8');
            });

            it('Check complex calculation.', function () {
                browser.actions().click(button_1).perform();
                browser.actions().click(button_plus).perform();
                browser.actions().click(button_2).perform();
                browser.actions().click(button_multiplication).perform();
                browser.actions().click(button_3).perform();
                browser.actions().click(button_plus).perform();
                browser.actions().click(button_2).perform();
                browser.actions().click(button_multiplication).perform();
                browser.actions().click(button_0).perform();
                browser.actions().click(button_calculate).perform();
                expect(display.getText()).toBe('= 7');
                expect(outputFormula.getText()).toBe('1 + 2 * 3 + 2 * 0');
            });
        });
    });
}());