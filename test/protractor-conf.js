(function () {
    'use strict';

    exports.config = {
        allScriptsTimeout: 11000,

        specs: [
            'e2e/*.js'
        ],

        capabilities: {
            'browserName': 'chrome'
        },

        chromeOnly: true,

        baseUrl: 'http://localhost:8080/',

        framework: 'jasmine',

        jasmineNodeOpts: {
            defaultTimeoutInterval: 30000
        },
        onPrepare: function () {
            browser.manage().timeouts().pageLoadTimeout(40000);
            browser.manage().timeouts().implicitlyWait(25000);
        }
    };
}());